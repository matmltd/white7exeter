<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>White 7 Exeter</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<link rel="icon" href="favicon.ico" type="image/icon" sizes="16x16">
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" >
<link href="css/style.less" type="text/css" rel="stylesheet/less" >
<script src="less.min.js" type="text/javascript"></script>
<link href="font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet" >
<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
<script src="slider/jquery.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="slider/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="slider/jquery.bxslider.css" rel="stylesheet" />



</head>

<body>

<?php include("header.php"); ?>
<section>
  <div class="sliderOuter">
    <div class="container">
    <div class="row">
      <ul class="bxslider">
        <li><img src="images/slider1.jpg" alt="White 7 Exeter"  /></li>

        <li><img src="images/slider5.jpg"  alt="White 7 Exeter"/></li>
        <li><img src="images/slider4.jpg" alt="White 7 Exeter" ></li>
      </ul>
      </div>
    </div>
  </div>
</section>
<section>
  <article class="container homepageboxes">
    <div class="row">
      <div class="col-lg-4">
      
        <h1>Wedding </h1>
      <div class="boximg"> <a href="wedding.php"> <img src="images/Wedding.jpg" width="313" height="198" alt="Wedding"/> </a> </div></div>
      <div class="col-lg-4">
        <h2>Experiences </h2>
       <div class="boximg">  <a href="experiences.php"> <img src="images/Experiences.jpg" width="313" height="198" alt="Experiences"/> </a> </div> </div>
      <div class="col-lg-4">
        <h3>Corporate </h3>
      <div class="boximg">  <a href="corporate.php">  <img src="images/Corporate.jpg" width="313" height="198" alt="Corporate"/> </a> </div> </div>
    </div>
  </article>
</section>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-7">
        <h1>Welcome to White 7 Exeter </h1>
        <p>An Exeter area man has launched a new chauffeuring business in the region after teaming up with Midlands-based chauffeuring experience company White 7 UK.</p>
        <p>White 7 is working towards creating a national brand, having already launched franchises franchises in Guildford and Shrewsbury providing BMW 7 series limousines using a fleet of white long wheelbase vehicles...</p>
        <a href="about-white7-exester.php" class="readmore">Read more...</a> </div>
      <div class="col-lg-3">
        <h5>Awards</h5>
        <ul>
          <li><a href="http://www.white7.co.uk/white-7-brings-gold-award-back-to-the-county/" target="_blank">White 7 Brings Gold Award Back To The County</a></li>
        </ul>
      </div>
      <div class="col-lg-2">
        <h6>News </h6>
          <ul>
        <li> <a href="http://www.white7.co.uk/testimonial/" title="Go to Testimonial" rel="bookmark" target="_blank">Testimonial</a> </li>
        <li> <a href="http://www.white7.co.uk/new-arrivals/" title="Go to  New Arrivals" rel="bookmark" target="_blank">New Arrivals</a> </li>
        <li> <a href="http://www.white7.co.uk/mayors-recognition/" title="Go to  Mayor’s Recognition" rel="bookmark" target="_blank">Mayor’s Recognition</a> </li>
        <li> <a href="http://www.white7.co.uk/shooters-hill-halll/" title="Go to   Shooters Hill Hall" rel="bookmark" target="_blank">Shooters Hill Hall</a> </li>
        <div class="cl"></div>
      </ul>
      </div>
      
      <div class="cl"></div>
      
      <div class="copyrights col-lg-6">&copy; <span class="numbers"> 2016 </span> White 7 Exeter  Telephone : <span class="numbers">01460 54829 </span></div>
<div class="col-lg-6 smallmenu"><a href="#">Terms & Conditions </a>|  <a href="sitemap.php">Sitemp</a></div>      
      
    
    </div>
  </div>
</footer>
</body>
<script>
$(document).ready(function(){
  $('.bxslider').bxSlider();
});
</script>
</html>
