<header class="container">
  <div class="row">
    <div class="col-lg-4 logo"><a href="index.php"><img src="images/logo.jpg" width="161" height="137" alt="White 7 Exeter" title="White 7 Exeter"/></a></div>
    <div class="col-lg-8">
      <div class="social"> <a href="https://www.facebook.com/pages/White-7/332879043522236" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="https://twitter.com/white7_" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a> </div>
      <script>
$(document).ready(function() {

	$('#menu-toggle').click(function () {
      $('#menu').toggleClass('open');
      e.preventDefault();
    });
    
});
</script>
      <a id="menu-toggle" class="anchor-link"><i class="fa fa-bars" aria-hidden="true"></i><span class="i-name">Navigation</span></a>

      <div class="topmenu cl">
        <nav id="menu">
          <ul>
            <li><a href="contact-us.php">Contact Us </a></li>
            <li><a href="corporate.php">Corporate </a></li>
            <li><a href="experiences.php">Experiences</a></li>
            <li><a href="wedding.php">Wedding </a></li>
            <li><a href="index.php">Home</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</header>