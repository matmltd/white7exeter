<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Corporate  -  White 7 Exeter</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<link rel="icon" href="favicon.ico" type="image/icon" sizes="16x16">
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" >
<link href="css/style.less" type="text/css" rel="stylesheet/less" >
<script src="less.min.js" type="text/javascript"></script>
<link href="font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet" >
<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
<script src="slider/jquery.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="slider/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="slider/jquery.bxslider.css" rel="stylesheet" />
<script>
$(document).ready(function(){
  $('.bxslider').bxSlider();
});
</script>
</head>

<body>
<?php include("header.php"); ?>

<section class="InnerOUter">

<div class="container">

<div class="white-box corporates">
<h2>Corporate </h2>
<div class="col-lg-7"><p>White 7 Exeter has been designed to accommodate business travel. Our motto is comfort, convenience and confidentiality which is the life blood to create longevity between us and the client.</p>

<p>As the door is opened for you and you sit inside one of our BMW fleet you can relax as the journey begins. For your comfort and safety, the BMW 730 M Sport is designed for the business executive to travel in comfort and style and when coupled with a professionally qualified chauffeur  you will be prepared to work on the move.</p>

<p>Convenience and confidentiality allows the client and /  or their guests to be taken from ‘ door to door ‘ and without the stress, prescribed timetables and lack of privacy associated with public transport. With on board drinks to keep you refreshed, you will arrive at your destination suitably prepared and ready to do business.</p>

<p>White 7 can be used as part of your marketing budget and can pick up clients from Heathrow, Gatwick, Manchester and Birmingham Airports to ensure their safe travel directly to your premises. Also, your executives can use this service for internal staff motivation and reward.</p>

<a href="contact-us.php" class="engBtn">Enquire</a>
</div>

<div class="col-lg-5">
<p>  <img src="images/Corporate4.jpg" width="382" alt=""/> </p>

<img src="images/Corporate2.jpg" width="382" height="295" alt=""/> </div>


<div class="cl"></div><br>

<div class="row">

<div class="col-lg-12">  <img src="images/Corporate3.jpg" width="100%" alt=""/> </div>
</div>
<div class="cl"></div>  
 </div>
</div>
</section>
<?php include("footer_inner.php"); ?>


</body>
</html>
