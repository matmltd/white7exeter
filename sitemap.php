<?php 

$msg = '';

if(isset($_REQUEST['msg'])) {

	if($_REQUEST['msg'] == 1) {

		$msg = "Your mail has been successfully.";

	} elseif($_REQUEST['msg'] == 301) {

		$msg = "Entered captcha not valid.";

	} else {

		$msg = "Mail  is not send.";

	}

}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Contact  -  White 7 Exeter</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<link rel="icon" href="favicon.ico" type="image/icon" sizes="16x16">
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" >
<link href="css/style.less" type="text/css" rel="stylesheet/less" >
<script src="less.min.js" type="text/javascript"></script>
<link href="font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet" >
<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
<script src="slider/jquery.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="slider/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="slider/jquery.bxslider.css" rel="stylesheet" />
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">

$(document).ready(function() {

	$("#button").click(function() {
	

		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		
 if($("#name").val() == '') {

			$("#msg").html('Please enter  Name.');

			$("#name").focus();

			return false;
			
	} else if($("#email").val() == '') {

			$("#msg").html('Please enter Email.');

			$("#email").focus();

			return false;

	} else if(!emailReg.test($("#email").val())) {

			$("#msg").html('Please enter valid email.');

			$("#email").focus();

			return false;	
			
			
	} else if($("#telephone").val() == '') {

			$("#msg").html('Please enter  Contact Number');

			$("#telephone").focus();

			return false;

	}	else if($("#services").val() == '0') {

			$("#msg").html('Please Select Services.');

			$("#services").focus();

			return false;
			
	}	else if($("#collection").val() == '') {

			$("#msg").html('Please Enter Collection Address Details');

			$("#collection").focus();

			return false;
			
	}	else if($("#destination").val() == '') {

			$("#msg").html('Please Enter Destination Address ');

			$("#destination").focus();

			return false;
	}	else if($("#returncollection").val() == '') {

			$("#msg").html('Please Enter Return Journey Collection Address');

			$("#returncollection").focus();

			return false;
	}	else if($("#returncollection").val() == '') {

			$("#msg").html('Please Enter  Return Journey Destination Address ');

			$("#returncollection").focus();

			return false;
	
	} else if($("#security_code").val() == '') {

			$("#msg").html('Please Enter 5 Digit code');

			$("#security_code").focus();

			return false;
			
			
			

		} else {

			return true;	

		}

			

	});

	

	$("#cancel").click(function() {

								$("#services").val('');

								$("#name").val('');

								$("#email").val('');

								$("#cname").val('');

								$("#phone").val('');

								$("#inquiry").val('');

								$("#msg").html('');

							

								});

});

</script>
</head>

<body>
<?php include("header.php"); ?>

<section class="InnerOUter">

<div class="container">

<div class="white-box">

<div class="row">
<ul>
           
            <li><a href="index.php">Home</a></li>
            <li><a href="corporate.php">Corporate </a></li>

            <li><a href="experiences.php">Experiences</a></li>
            <li><a href="wedding.php">Wedding </a></li>
                         <li><a href="contact-us.php">Contact Us </a></li>
          </ul>
</div>

 <div class="cl"></div>
 </div>

</div>
</section>
<?php include("footer_inner.php"); ?>


</body>
</html>
