<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Wedding  -  White 7 Exeter</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<link rel="icon" href="favicon.ico" type="image/icon" sizes="16x16">
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" >
<link href="css/style.less" type="text/css" rel="stylesheet/less" >
<script src="less.min.js" type="text/javascript"></script>
<link href="font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet" >
<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
<script src="slider/jquery.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="slider/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="slider/jquery.bxslider.css" rel="stylesheet" />
<script>
$(document).ready(function(){
  $('.bxslider').bxSlider();
});
</script>
</head>

<body>
<?php include("header.php"); ?>

<section class="InnerOUter">

<div class="container">

<div class="white-box">
<h2>Wedding </h2>

<div class="col-lg-7"><p>A wedding day should be a perfect day for the bride, her groom and their families and friends.</p>

<p>We take great pride in providing the very best chauffeured wedding service, thanks to our fleet of gleaming new model luxury BMW cars and our professionally qualified female and male chauffeurs.</p>

<p>Where ever your wedding venue might be, we can take you there in style and comfort, with the certainty that we will get you there on time.</p>

<p><strong class="bigtext" style="color:#3d3b3b;">“If this is your big day let <br />

<div style="text-align:right; display:block;">
White 7 make  it your perfect day.”</div></strong></p>




</div>

<div class="col-lg-5"><img src="images/wedding-img.jpg" width="100%" alt=""/></div>
<div class="cl"></div>



<div class="cl"></div>
<div class="lightgray">
<h2>The Ultimate Wedding Experience</h2>
<p>
A Day of Celebration: A Wedding day should be a perfect day for the bride, her groom their families and friends.</p>
<p>

We take great pride in providing the very best chauffeured wedding service, thanks to our fleet of gleaming new model
luxury BMW cars and our professionally qualified female and male chauffeurs. </p>
<p>

Where ever your wedding venue might be, we can take you there in style and comfort, with the certainty that we will get
you there on time. If this is your big day – let White 7 make it your perfect day.</p>


<div class="serviceMain">

<div class="serviceListing">
<div class="col-lg-8">
<h5>The Limousine Service</h5>

<span class="red">£275.00* up to 4 guests.</span>

<p>The limousine service incorporating one chauffeured vehicle: <br />

BMW 730 LWB SE</p>
</div>
<div class="col-lg-4">
<div class="arrowmain"></div>
<div class="serviceRight"><span>01</span>Limousine </div>
</div>
<div class="cl"></div>

</div>



<div class="serviceListing">
<div class="col-lg-8">
<h5>The Ribbon Service</h5>

<span class="red">£525.00* up to 8 guests.</span>

<p>The ribbon service incorporating two chauffeured vehicles:  <br />

BMW 730 LWB SE •  BMW 730 M Sport or BMW 530 GT M Sport  </p>
</div>
<div class="col-lg-4">
<div class="arrowmain"></div>
<div class="serviceRight"><span>02</span>Ribbon </div>
</div>
<div class="cl"></div>

</div>


<div class="serviceListing">
<div class="col-lg-8">
<h5>The Champagne Service</h5>

<span class="red">Price available upon request. Up to 12 guests</span>

<p>The champagne service incorporating three chauffeured vehicles:<br />

BMW 730 LWB SE  • BMW 730 M Sport •BMW 530 GT M Sport</p>
</div>
<div class="col-lg-4">
<div class="arrowmain"></div>
<div class="serviceRight"><span>03</span>Champagne </div>
</div>
<div class="cl"></div>

</div>



<div class="serviceListing">
<div class="col-lg-8">
<h5>The Complete Service</h5>

<span class="red">Price available upon request. Up to 16 guests.</span>

<p>The complete service incorporating four chauffeured vehicles: <br />
BMW 730 LWB SE  • BMW 730 M Sport  • BMW 530 GT M Sport • BMW 5 Series SE</p>
</div>
<div class="col-lg-4">
<div class="arrowmain"></div>
<div class="serviceRight"><span>04</span>Complete </div>
</div>
<div class="cl"></div>

</div>






</div>
<p><strong>NB</strong> :  All prices are correct at the time of print but mileage limits may apply. All prices above exclude vat.<br> </p>


<a href="contact-us.php" class="engBtn">Enquire</a>

</div>

 </div>
</div>
</section>
<?php include("footer_inner.php"); ?>


</body>
</html>
