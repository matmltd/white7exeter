<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>About White 7 Exeter</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<link rel="icon" href="favicon.ico" type="image/icon" sizes="16x16">
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" >
<link href="css/style.less" type="text/css" rel="stylesheet/less" >
<script src="less.min.js" type="text/javascript"></script>
<link href="font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet" >
<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
<script src="slider/jquery.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="slider/jquery.bxslider.js"></script>
<!-- bxSlider CSS file -->
<link href="slider/jquery.bxslider.css" rel="stylesheet" />
<script>
$(document).ready(function(){
  $('.bxslider').bxSlider();
});
</script>
</head>

<body>
<?php include("header.php"); ?>

<section class="InnerOUter">

<div class="container">

<div class="white-box">
<h2>About White 7 Exeter </h2>

<div class="col-lg-7"><p>

An <strong>Exeter area</strong> man has launched a new chauffeuring business in the region after teaming up with Midlands-based chauffeuring experience company White 7 UK.  </p>
<p>White 7 is working towards creating a national brand, having already launched franchises franchises in Guildford and Shrewsbury providing BMW 7 series limousines using a fleet of white long wheelbase vehicles, each matched with its own distinctive White 7 branding and subtle attention to detail. </p>
<p>
<strong>Trevor Larcombe</strong>, aged 59, is a former engineer from the market town of Ilminster, in Somerset, having worked for the Westland group, before deciding to start this new business venture.</p>
<p>
<strong> Trevor, commented:</strong>

 “I have always been a keen driver myself and recognised this new franchise as being an ideal way to make a living by doing something I love.  </p>
<p>
“By becoming managing director of my own company I will gain responsibility for the delivery of a high quality service to my clients. </p>
<p>
“I will initially cover Exeter, Yeovil, Taunton and the surrounding areas focussing on Weddings, Corporate Clients and Experiences. I am a member of the British Chauffeurs Guild and that will enhance my chauffeur skills when looking after my clients. White 7 UK Managing Director Alan Bowyer, added: “With his attitude and professionalism Trevor is an ideal candidate to help extend the White 7 brand nationwide and he has a really beautiful area of the country to work in. </p>
<p>
“During the Summer of <span class="numbers">2016</span> I supported fellow franchisee David French in Shropshire with numerous weddings as part of my extensive White 7 chauffeur training. I am now looking forward to replicating that service in my home county of Somerset." </p>
</div>
<div class="col-lg-5"><p><img src="images/about-us.jpg"  height="auto" alt="Trevor Larcombe with Alan Bowyer and Anthony Randall "/></div>

<div class="cl"></div>


<p>
‘ We are absolutely delighted at the level of interest and progress of the company since being launched just two years ago.’ White 7 can be contacted at <a href="http://www.white7.co.uk/" target="_blank">www.white7.co.uk </a> </p>




 </div>
</div>
</section>
<?php include("footer_inner.php"); ?>


</body>
</html>
